import { produce } from "immer";
import { AnyAction } from "redux";
import { AppStateType } from "../types";
import { AppConstants } from "../constants";

const initialState: AppStateType = {
	token: null,
};

const AppReducer = (state = initialState, action: AnyAction) =>
	produce(state, (draft) => {
		switch (action.type) {
			case AppConstants.SET_TOKEN:
				draft.token = action.payload;
				break;
			case AppConstants.DELETE_TOKEN:
				draft.token = null;
				break;
			default:
				return state;
		}
	});

export default AppReducer;
