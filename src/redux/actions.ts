import { UsersConstants, AppConstants } from "./constants";
import { User } from "../models/Users";

export const setUsersRedux = (users: Array<User>) => {
	return {
		type: UsersConstants.SET_USERS,
		payload: users,
	};
};

export const deleteUserRedux = (users: Array<User>) => {
	return {
		type: UsersConstants.DELETE_USER,
		payload: users,
	};
};

export const setPages = (pages: number) => {
	return {
		type: UsersConstants.SET_PAGES,
		payload: pages,
	};
};

export const setToken = (token: string) => {
	return {
		type: AppConstants.SET_TOKEN,
		payload: token,
	};
};

export const deleteToken = () => {
	return {
		type: AppConstants.DELETE_TOKEN,
	};
};
