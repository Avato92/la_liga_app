import { createStore, applyMiddleware, Store } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";

const middlewares = [thunk];

const enhancers = [applyMiddleware(...middlewares)];

const store: Store<any, never> & {
	dispatch: unknown;
} = createStore(rootReducer(), enhancers);

export default store;
