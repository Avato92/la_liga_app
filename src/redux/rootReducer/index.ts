import { combineReducers, Reducer } from "redux";
import { GlobalReducerStateType } from "../types";
import UsersReducer from "../reducers/userReducer";
import AppReducer from "../reducers/appReducer";

const createReducer = (injectedReducer = {}): Reducer<any, never> => {
	return combineReducers<GlobalReducerStateType>({
		users: UsersReducer,
		app: AppReducer,
		...injectedReducer,
	});
};

export default createReducer;
