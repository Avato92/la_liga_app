# Prueba técnica para La Liga

Proyecto realizado por **Alejandro Vañó Tomás** para La Liga.

## Objetivos obligatorios:

- [x] React con typescript
- [x] hooks
- [x] react-router
- [x] Preprocesador SCSS
- [x] Breakpoint
- [x] Redux
- [x] Middleware (thunk y immer)
- [x] eslint y prettier
- [x] CRUD
- [x] Control de errores CRUD
- [x] Login
- [x] Uso del token
- [x] Control páginas sin token
- [x] Documentación

## Objetivos opcionales

- [ ] Uso de saga
- [ ] styled-components
- [ ] githook
- [x] pagination
- [x] testing

## Scripts

---

Para lanzar la aplicación en local en modo desarrollo.

### `npm start`

Abrir [http://localhost:3000](http://localhost:3000) en el navegador.

---

### `npm run build`

Genera la aplicación para su lanzamiento de producción y la almacena dentro de la carpeta `build`.

---

### `npm test`

Ejecuta los test de la aplicación.

### `npm run lint`

Comprueba los archivos y si están bien escritos o tienen algún error de sintaxis.

---

### `npm run lint:fix`

Arregla los errores sintácticos.

## Librerías externas usadas

- [`axios`](https://axios-http.com/docs/intro), librería para realizar peticiones HTTP.
- [`node-sass`](https://sass-lang.com/documentation/js-api), para el uso de SCSS.
- [`husky`](https://typicode.github.io/husky/#/), necesaria para el uso de git hooks.
- `redux`
- `react-redux`
- `eslint`
- `Prettier`
- `typescript`
- Librerías de tipado para el correcto funcionamiento de react y redux con typescript.
- `react-router`
- [`react-toastify`](https://www.npmjs.com/package/react-toastify), librería para generar alertas y poder dar feedback al usuario.
- [`react-pagination`](npmjs.com/package/react-paginate)
- [`immer`](https://immerjs.github.io/immer/), librería para la inmutabilidad del estado de redux y un mejor manejo.
- `redux-thunk`
- `enzyme`, librería para testear.
- `enzyme-adapter-16`
- `enzyme-to-json`, librería para el testing de snapshots.

## Explicaciones

En el archivo `/src/services/aixos.tsx`, he usado el tipo `any`, el motivo es que se supone que es un servicio que se puede aprovechar a lo largo
de toda la aplicación y son las peticiones básicas, por lo tanto en el tipo POST y PUT, no se puede saber con certeza el tipo de dato que llegará
ya que si se usa en diferentes sitios pueden contener campos distintos.

En la documentación que me han dado dice que como router debo usar el `react-router`, este router está pensado para aplicaciones nativas y ser usado
con `react native`, en su contra usaré `react-router-dom`, que si está hecho para aplicaciones hechas con `react`.

Para la denominación de las clases he usado la metodología BEM que en mi opinión es una buena practica y se acopla muy bien con SCSS.

Se ha generado un archivo de mixins donde se ha añadido un mix para hacer un breakpoint a un tamaño concreto, si en un futuro la aplicación se quisiera escalar
y añadir más tipos de breakpoints para adaptarse a otros dispositivos, solo habría que generar otro mixin con el tamaño de break que se desee.

Se ha hecho una pequeña implementación de redux, intentando adaptarse a la API facilitada como ejemplo del uso, obviamente el uso real no es el mismo, pero creo que
es un buen ejemplo de que poseo conocimientos necesarios para implementar Redux.

Se ha utilizado la librería `enzyme` para hacer testing.
