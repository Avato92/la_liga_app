import React, { ChangeEvent, useEffect, useState } from "react";
import user_white from "../../assets/images/user_white.png";
import "./style/login.scss";
import { useHistory } from "react-router-dom";
import AxiosService from "../../services/axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = () => {
	const [email, setEmail] = useState<string>("");
	const [password, setPassword] = useState<string>("");
	const history = useHistory();

	toast.configure();

	useEffect(() => {
		const token = localStorage.getItem("token");
		token && history.push("/list");
	}, [history]);

	const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
		setEmail(event.target.value);
	};
	const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
		setPassword(event.target.value);
	};
	const handleClick = (event: React.FormEvent<HTMLInputElement>) => {
		event.preventDefault();
		AxiosService.post("/login", { email: email, password: password }).then(
			(res) => {
				if (res.Data) {
					localStorage.setItem("token", res.Data.token);
					history.push("/list");
					window.location.reload();
				} else {
					toast.error("Usuario no encontrado");
					setEmail("");
					setPassword("");
				}
			}
		);
	};

	return (
		<div className="login">
			<img className="login__img-main" src={user_white} alt=""></img>
			<form className="login__form" autoComplete="off">
				<h2 className="login__title">Log In:</h2>
				<input
					placeholder="Email"
					className="login__input-email"
					autoComplete="false"
					onChange={handleEmailChange}
					value={email}
				/>
				<input
					type="password"
					placeholder="Password"
					className="login__input-password"
					autoComplete="false"
					onChange={handlePasswordChange}
					value={password}
				/>
				<input
					type="submit"
					className="login__button-submit"
					onClick={handleClick}
					disabled={!email || !password}
					value="Entrar"
				/>
			</form>
		</div>
	);
};

export default Login;
