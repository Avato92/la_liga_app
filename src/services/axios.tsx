import Response from "../models/Response";
import axios from "axios";

export default class AxiosService {
	private static baseURL = "https://reqres.in/api";

	public static async getAll<T>(url: string): Promise<Response> {
		const res = await axios
			.get<Array<T>>(this.baseURL + url)
			.then((response) => {
				return new Response(true, response.data as Array<T>, "Éxito", "");
			})
			.catch((error) => {
				return new Response(false, null, "Error", error);
			});
		return res;
	}

	public static async get<T>(url: string, param: number): Promise<Response> {
		const res = await axios
			.get<T>(this.baseURL + url + "/" + param)
			.then((response) => {
				return new Response(true, response.data, "Éxito", "");
			})
			.catch((error) => {
				return new Response(false, null, "Error", error);
			});
		return res;
	}

	public static async post<T>(url: string, data: any): Promise<Response> {
		const res = await axios
			.post<T>(this.baseURL + url, data)
			.then((response) => {
				return new Response(true, response.data, "Éxito", "");
			})
			.catch((error) => {
				return new Response(false, null, "Error", error);
			});
		return res;
	}

	public static async put<T>(url: string, data: any): Promise<Response> {
		const res = await axios
			.put<T>(this.baseURL + url, data)
			.then((response) => {
				return new Response(true, response.data, "Éxito", "");
			})
			.catch((error) => {
				return new Response(false, null, "Error", error);
			});
		return res;
	}

	public static async delete(url: string, param: number): Promise<Response> {
		const res = await axios
			.post(this.baseURL + url + "/" + param)
			.then((response) => {
				return new Response(true, null, "Éxito", "");
			})
			.catch((error) => {
				return new Response(false, null, "Error", error);
			});
		return res;
	}
}
