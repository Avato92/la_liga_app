import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "../containers/Login";
import List from "../containers/List";
import User from "../containers/User";

const Routes = () => {
	return (
		<Router>
			<Switch>
				<Route path="/list" component={List} />
				<Route path="/user/:id" component={User} />
				<Route path="/" component={Login} />
			</Switch>
		</Router>
	);
};

export default Routes;
