import React from "react";
import App from "../App";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import Logout from "../assets/images/logout.png";

describe("App render", () => {
	it("renders without crashing", () => {
		shallow(<App />);
	});
	it("have 3 divs inside", () => {
		const component = shallow(<App />);
		expect(component.find("div")).toHaveLength(3);
	});
	it("Logout btn not render", () => {
		const nullFunction = () => {
			console.log("hola mundo");
		};
		const main = shallow(<App />);
		const btn = (
			<button className="logout__btn" onClick={nullFunction}>
				<img className="logout__img" alt="Botón de log out" src={Logout} />
			</button>
		);
		expect(main.contains(btn)).toEqual(false);
	});
});

describe("snapshot", () => {
	it("App snapshots", () => {
		const tree = shallow(<App />);
		expect(toJson(tree)).toMatchSnapshot();
	});
});
