module.exports = {
	root: true,
	parser: "@typescript-eslint/parser",
	plugins: ["@typescript-eslint", "jest"],
	extends: [
		"react-app",
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:jest/recommended",
		"plugin:prettier/recommended",
		"prettier",
		"plugin:import/recommended",
		"plugin:import/typescript",
	],
	rules: {
		"@typescript-eslint/explicit-module-boundary-types": "off",
	},
};
