import { User } from "../../models/Users";

export type GlobalReducerStateType = {
	users: UsersStateType;
	app: AppStateType;
};

export type UsersStateType = {
	users: Array<User>;
	pages: number;
};

export type AppStateType = {
	token: string | null;
};
