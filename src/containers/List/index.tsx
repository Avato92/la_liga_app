import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import AxiosService from "../../services/axios";
import Loading from "../Loading";
import { User } from "../../models/Users";
import { toast } from "react-toastify";
import "./style/list.scss";
import ReactPaginate from "react-paginate";
import { useDispatch, useSelector } from "react-redux";
import { GlobalReducerStateType } from "../../redux/types";
import { setUsersRedux, setPages } from "../../redux/actions";

const List = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const [users, setUsers] = useState<Array<User> | null>(null);
	const [loading, setLoading] = useState<boolean>(true);
	const [totalPages, setTotalPages] = useState<number>(0);

	const usersRedux = useSelector(
		(state: GlobalReducerStateType) => state?.users
	);

	useEffect(() => {
		if (usersRedux.users.length <= 0) {
			AxiosService.getAll("/users").then((res) => {
				setLoading(false);
				setTotalPages(res.Data.total_pages);
				if (res.Messages === "Éxito") {
					setUsers(res.Data.data);
					dispatch(setUsersRedux(res.Data.data));
					dispatch(setPages(res.Data.total_pages));
				} else {
					toast.error("Ocurrió un error, por favor intentelo más tarde.");
				}
			});
		}
		setLoading(false);
	}, []);

	useEffect(() => {
		usersRedux?.users && setUsers(usersRedux.users);
	}, [usersRedux]);

	useEffect(() => {
		const token = localStorage.getItem("token");
		!token && history.push("/");
	}, [history]);

	const handleClick = (id: number) => {
		history.push("/user/" + id);
	};

	const changePage = ({ selected }: any) => {
		const page = selected + 1;
		AxiosService.getAll("/users?page=" + page).then((res) => {
			res.Messages === "Éxito"
				? setUsers(res.Data.data)
				: toast.error("Ocurrió un error, por favor intentelo más tarde.");
		});
	};
	return (
		<div className="list">
			{loading && <Loading />}
			{users && users.length <= 0 && <h2>No hay usuarios registrados</h2>}
			{users &&
				users.map((user, index) => {
					return (
						<div
							key={index}
							className="list__card"
							onClick={() => handleClick(user.id)}
						>
							<img
								src={user.avatar}
								className="list__card-avatar"
								alt={user.first_name + " photo"}
							/>
							<p className="list__text list__text-name">{user.first_name}</p>
							<h2 className="list__text list__text-surname">
								{user.last_name}
							</h2>
						</div>
					);
				})}
			;
			<ReactPaginate
				previousLabel={"<"}
				nextLabel={">"}
				pageCount={totalPages || usersRedux?.pages}
				onPageChange={changePage}
				pageRangeDisplayed={2}
				marginPagesDisplayed={2}
				containerClassName={"pagination__btns"}
				previousClassName={"pagination__btn-prev"}
				nextLinkClassName={"pagination__btn-nxt"}
				disabledClassName={"pagination__btn-disabled"}
				activeClassName={"pagination__btn"}
			/>
		</div>
	);
};

export default List;
