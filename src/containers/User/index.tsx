import React, { ChangeEvent, useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import AxiosService from "../../services/axios";
import { User as UserModel } from "../../models/Users";
import { toast } from "react-toastify";
import Loading from "../Loading";
import "./style/user.scss";
import back from "../../assets/images/atras.png";
import { useSelector, useDispatch } from "react-redux";
import { GlobalReducerStateType } from "../../redux/types";
import { deleteUserRedux } from "../../redux/actions";

const User = () => {
	const { id } = useParams();
	const history = useHistory();
	const [loading, setLoading] = useState<boolean>(true);
	const [user, setUser] = useState<UserModel | null>(null);
	const dispatch = useDispatch();

	const { users } = useSelector(
		(state: GlobalReducerStateType) => state?.users
	);

	toast.configure();

	useEffect(() => {
		const token = localStorage.getItem("token");
		!token && history.push("/");
		AxiosService.get("/users", id).then((res) => {
			setLoading(false);
			if (res.Messages === "Error") {
				toast.error("Ocurrió un error, intentelo más tarde!");
				history.push("/list");
			} else {
				setUser(res.Data.data);
			}
		});
	}, [id, history]);

	const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
		if (user?.id) {
			const newUser: UserModel = { ...user, email: event.target.value };
			setUser(newUser);
		}
	};
	const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
		if (user?.id) {
			const newUser: UserModel = { ...user, first_name: event.target.value };
			setUser(newUser);
		}
	};
	const handleSurnameChange = (event: ChangeEvent<HTMLInputElement>) => {
		if (user?.id) {
			const newUser: UserModel = { ...user, last_name: event.target.value };
			setUser(newUser);
		}
	};
	const handleClick = (event: React.FormEvent<HTMLInputElement>) => {
		event.preventDefault();
		if (!user?.email) {
			toast.warning("Imposible actualizar, introduzca email.");
		} else if (!user?.first_name) {
			toast.warning("Imposible actualizar, introduzca nombre.");
		} else if (!user?.last_name) {
			toast.warning("Imposible actualizar, introduzca apellido.");
		} else {
			AxiosService.put("/users/" + user.id, user).then((res) => {
				if (res.Messages === "Éxito") {
					toast.success("Usuario actualizado!");
					history.push("/list");
				} else {
					toast.error("Algo salió mal, inténtelo más tarde.");
				}
			});
		}
	};

	const handleDeleteClick = (event: React.FormEvent<HTMLInputElement>) => {
		event.preventDefault();
		AxiosService.delete("/users", id).then((res) => {
			if (res.Messages === "Error") {
				toast.error(
					"No se ha podido eliminar el usuario, inténtelo más tarde."
				);
			} else {
				const usersFiltered = users.filter((user) => user.id != id);
				dispatch(deleteUserRedux(usersFiltered));
				toast.success("Usuario eliminado");
				history.push("/list");
			}
		});
	};

	const handleBack = () => {
		history.push("/list");
	};

	return (
		<div>
			<button className="back__btn" type="submit" onClick={handleBack}>
				<img className="back__img" src={back} alt="Volver atrás" />
			</button>
			<div className="details">
				{loading && <Loading />}
				{user && (
					<>
						<img
							src={user.avatar}
							className="details__img"
							alt={user.first_name + " photo"}
						/>
						<form className="details__container">
							<div className="details__text">
								<label className="details__label">Email</label>
								<input
									value={user.email}
									className="details__text details__text-email"
									onChange={handleEmailChange}
								/>
							</div>
							<div className="details__text">
								<label className="details__label">Nombre</label>
								<input
									value={user.first_name}
									className="details__text-name"
									onChange={handleNameChange}
								/>
							</div>
							<div className="details__text">
								<label className="details__label">Apellido</label>
								<input
									value={user.last_name}
									className="details__text-surname"
									onChange={handleSurnameChange}
								/>
							</div>
							<div className="details__container-buttons">
								<input
									className="details__btn details__btn-update"
									type="submit"
									value="Actualizar"
									onClick={handleClick}
								/>
								<input
									className="details__btn details__btn-delete"
									type="submit"
									value="Eliminar"
									onClick={handleDeleteClick}
								/>
							</div>
						</form>
					</>
				)}
			</div>
		</div>
	);
};

export default User;
