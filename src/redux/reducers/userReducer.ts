import { produce } from "immer";
import { AnyAction } from "redux";
import { UsersStateType } from "../types";
import { UsersConstants } from "../constants";

const initialState: UsersStateType = {
	users: [],
	pages: 0,
};

const UsersReducer = (state = initialState, action: AnyAction) =>
	produce(state, (draft) => {
		switch (action.type) {
			case UsersConstants.SET_USERS:
				draft.users = action.payload;
				break;
			case UsersConstants.DELETE_USER:
				draft.users = action.payload;
				break;
			case UsersConstants.SET_PAGES:
				draft.pages = action.payload;
				break;
			default:
				return state;
		}
	});

export default UsersReducer;
