import React from "react";
import Login from "../containers/Login";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

describe("Tests login component", () => {
	it("Components renders", () => {
		shallow(<Login />);
	});
	it("Check text value", () => {
		const wrapper = shallow(<Login />);
		const label = wrapper.find(".login__title").text();
		expect(label).toEqual("Log In:");
	});
	it("Check input length", () => {
		const wrapper = shallow(<Login />);
		expect(wrapper.find("input")).toHaveLength(3);
	});
	it("Check if btn is disabled", () => {
		const wrapper = shallow(<Login />);
		const btn = wrapper.find(".login__button-submit").prop("disabled");
		expect(btn).toEqual(true);
	});
});

describe("snapshot", () => {
	it("Login snapshots", () => {
		const tree = shallow(<Login />);
		expect(toJson(tree)).toMatchSnapshot();
	});
});
