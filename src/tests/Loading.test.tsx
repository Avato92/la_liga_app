import React from "react";
import Loading from "../containers/Loading";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

describe("Test loading component", () => {
	it("renders", () => {
		shallow(<Loading />);
	});
	it("Check input length", () => {
		const wrapper = shallow(<Loading />);
		expect(wrapper.find("span")).toHaveLength(10);
	});
	it("Check text value", () => {
		const wrapper = shallow(<Loading />);
		const text = wrapper.find(".loading__word-6").text();
		expect(text).toEqual("n");
	});
});

describe("snapshot", () => {
	it("Loading snapshots", () => {
		const tree = shallow(<Loading />);
		expect(toJson(tree)).toMatchSnapshot();
	});
});
