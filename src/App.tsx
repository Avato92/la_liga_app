import React, { useEffect, useState } from "react";
import "./App.scss";
import Routes from "./routes";
import Logout from "./assets/images/logout.png";

const App = () => {
	const [token, setToken] = useState<string | null>(null);

	useEffect(() => {
		const tokenFromLocalStorage = localStorage.getItem("token");
		tokenFromLocalStorage && setToken(tokenFromLocalStorage);
	}, [token]);

	const handleClick = () => {
		localStorage.removeItem("token");
		setToken(null);
		window.location.reload();
	};

	return (
		<main className="background">
			<div className="background__circle-1"></div>
			<div className="background__circle-2"></div>
			<div className="background__glass_card">
				{token && (
					<button className="logout__btn" onClick={handleClick}>
						<img className="logout__img" alt="Botón de log out" src={Logout} />
					</button>
				)}
				<Routes />
			</div>
		</main>
	);
};

export default App;
