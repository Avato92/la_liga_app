export const UsersConstants = {
	SET_USERS: "SET_USERS",
	UPDATE_USER: "UPDATE_USER",
	DELETE_USER: "DELETE_USER",
	SET_PAGES: "SET_PAGES",
};

export const AppConstants = {
	DELETE_TOKEN: "DELETE_TOKEN",
	SET_TOKEN: "SET_TOKEN",
};
